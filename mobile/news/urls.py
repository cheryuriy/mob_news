from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from . import views


urlpatterns = format_suffix_patterns([
    url(r'^$', views.api_root),
    url(r'^users/$', views.GuestList.as_view(), name='guest-list'),
    url(r'^drafts/$', views.NewsDraftsList.as_view(), name='drafts-list'),
    url(r'^registration/$', views.Registration.as_view(), name='registration'),
    url(r'^news_all/$', views.NewsList.as_view(), name='news-list'),
    url(r'^authors/$', views.AuthorList.as_view(), name='author-list'),
    url(r'^categories/$', views.CategoryList.as_view(), name='category-list'),
    url(r'^tags/$', views.TagList.as_view(), name='tag-list'),
    url(r'^category/(?P<pk>[0-9]+)/$', views.CategoryDetail.as_view(), name='category-detail'),
    url(r'^news/(?P<pk>[0-9]+)/$', views.NewsDetail.as_view(), name='news-detail'),
])
