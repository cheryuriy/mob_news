from django.contrib import admin
from .models import *

admin.site.register(Guest)
admin.site.register(Author)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(News)
admin.site.register(Commentary)
admin.site.register(Photo)
